package org.example;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Data data[]={
                new Data(1, "Aa", 1, "Karyawan", 3000000),
                new Data(2, "Bebe", 12, "Manajer", 10000000),
                new Data(3, "Cece", 123, "Karyawan", 3000000),
                new Data(5, "Dede", 1234, "Atlit suit", 3000),
                new Data(7, "Ea", 12345, "Karyawan", 3000000)
        };

        for (Data d: data){
            System.out.println(d.getString());
        }
    }
}
