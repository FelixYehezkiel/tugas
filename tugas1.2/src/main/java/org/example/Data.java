package org.example;

public class Data {
  private   int idCard;
  private   String name;
  private   int idParking;
  private   String position;
  private   int salary;

    public Data (int idCard, String name, int idParking, String position, int salary){
        this.idCard=idCard;
        this.name=name;
        this.idParking=idParking;
        this.position=position;
        this.salary=salary;
    }

    public  String getString() {
        return "ID: "+ idCard + "\nNama: " + name + "\nNo Parkir: " + idParking +"\nJabatan: " +
                position + "\nGaji: " + salary*idCard + "\n";
    }


}
