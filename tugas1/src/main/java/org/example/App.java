
/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        IdCard idCardClass = new IdCard();
        IdParking idParkingClass = new IdParking();
        Salary salaryClass = new Salary();

        int id = 1;
        for (int i = 0; i < 5; i++) {
            idCardClass.setNama(i);
            idCardClass.getNama();

            salaryClass.setGaji(i);
            salaryClass.getGaji();

            idParkingClass.setIdParking(id);
            idParkingClass.getIdParking();

            System.out.println("ID: " + id + "\nNama: " + idCardClass.getNama() + "\nID Parkir: "
                    + idParkingClass.getIdParking() + "\nGaji: " + salaryClass.getGaji() * id);
            id++;
        }

    }
}
