package org.example;

import java.util.ArrayList;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ArrayList<String> goods1 = new ArrayList<>();
        goods1.add("a");
        goods1.add("b");
        goods1.add("c");
        goods1.add("d");
        goods1.add("e");

        ArrayList<String> goods2 = new ArrayList<>();
        goods2.add("e");
        goods2.add("f");

//        //Cara 1
//        ArrayList<String> newGoods = new ArrayList<String>(goods1);
//        newGoods.addAll(goods2);
//        ArrayList<String> substract = new ArrayList<String>(goods1);
//        substract.retainAll(goods2);
//
//        newGoods.removeAll(substract);
//        goods1.removeAll(goods1);
//        goods1.addAll(newGoods);
//
//
//        //Cara 2
//        ArrayList<String> retain = new ArrayList<>(goods1);
//        retain.retainAll(goods2);
//        goods1.removeAll(retain);
//        goods2.removeAll(retain);
//        goods1.addAll(goods2.removeAll(retain));


//        //Cara 3
//        ArrayList<String> retain = new ArrayList<>(goods2);
//        goods2.removeAll(goods1);
//        goods1.removeAll(retain);
//        goods1.addAll(goods2);

        System.out.println(goods1);
        for (String good : goods1){
            System.out.print(good + " ");
        }
    }
}
